<?php

include "conexion.php";

$user_id=null;
$sql1= "select * from usuarios where name like '%$_GET[s]%' or lastname like '%$_GET[s]%' or address like '%$_GET[s]%' or email like '%$_GET[s]%' or phone like '%$_GET[s]%' ";
$query = $con->query($sql1);
?>

<?php if($query->num_rows>0):?>
<table class="table table-bordered table-hover">
<thead>
	<th>Nombre</th>
	<th>Apellido Paterno</th>
	<th>Apellido Materno</th>
	<th>Fecha de Nacimiento</th>
	<th>Correo</th>
	<th>Sexo</th>
	<th>Escolaridad</th>
	<th>Ocupacion</th>
	<th>Comuna de Residencia</th>
	<th>Nombre de Calle</th>
	<th>Numero de Calle</th>
	<th>Numero de Departamento</th>
	<th>Numero de Celular</th>
	<th></th>
</thead>
<?php while ($r=$query->fetch_array()):?>
<tr>
	<td><?php echo $r["nombre"]; ?></td>
	<td><?php echo $r["ApellidoPaterno"]; ?></td>
	<td><?php echo $r["ApellidoMaterno"]; ?></td>
	<td><?php echo $r["FechaNacimiento"]; ?></td>
	<td><?php echo $r["Correo"]; ?></td>
	<td><?php echo $r["CodigoSexo"]; ?></td>
	<td><?php echo $r["CodigoEscolaridad"]; ?></td>
	<td><?php echo $r["CodigoOcupacion"]; ?></td>
	<td><?php echo $r["CodigoComuna"]; ?></td>
	<td><?php echo $r["NombreCalle"]; ?></td>
	<td><?php echo $r["NumeroCalle"]; ?></td>
	<td><?php echo $r["Departamento"]; ?></td>
	<td><?php echo $r["Celular"]; ?></td>
	<td style="width:150px;">
		<a href="./editar.php?id=<?php echo $r["id"];?>" class="btn btn-sm btn-warning">Editar</a>
		<a href="#" id="del-<?php echo $r["id"];?>" class="btn btn-sm btn-danger">Eliminar</a>
		<script>
		$("#del-"+<?php echo $r["id"];?>).click(function(e){
			e.preventDefault();
			p = confirm("Estas seguro?");
			if(p){
				window.location="./php/eliminar.php?id="+<?php echo $r["id"];?>;

			}

		});
		</script>
	</td>
</tr>
<?php endwhile;?>
</table>
<?php else:?>
	<p class="alert alert-warning">No hay resultados</p>
<?php endif;?>
