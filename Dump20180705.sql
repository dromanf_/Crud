CREATE DATABASE  IF NOT EXISTS `bd_drupal_ext` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `bd_drupal_ext`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: bd_drupal_ext
-- ------------------------------------------------------
-- Server version	5.7.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria` (
  `CódigoCategoria` int(11) NOT NULL,
  `NombreCategoria` varchar(45) NOT NULL,
  `Activo` varchar(45) NOT NULL,
  PRIMARY KEY (`CódigoCategoria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categoriacontenido (mcl)`
--

DROP TABLE IF EXISTS `categoriacontenido (mcl)`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoriacontenido (mcl)` (
  `IdContenidoLocales` int(11) NOT NULL,
  `CódigoCategoria` varchar(45) NOT NULL,
  `FechaRegistro` varchar(45) NOT NULL,
  KEY `FK_CategoriaContenido_ContenidoLocales` (`IdContenidoLocales`),
  CONSTRAINT `FK_CategoriaContenido_Categoria` FOREIGN KEY (`IdContenidoLocales`) REFERENCES `categoria` (`CódigoCategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_CategoriaContenido_ContenidoLocales` FOREIGN KEY (`IdContenidoLocales`) REFERENCES `contenidoslocales` (`IdContenidosLocales`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoriacontenido (mcl)`
--

LOCK TABLES `categoriacontenido (mcl)` WRITE;
/*!40000 ALTER TABLE `categoriacontenido (mcl)` DISABLE KEYS */;
/*!40000 ALTER TABLE `categoriacontenido (mcl)` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comunas`
--

DROP TABLE IF EXISTS `comunas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comunas` (
  `CódigoProvincia` int(11) NOT NULL,
  `CódigoComuna` int(11) NOT NULL,
  `CódigoPostal` varchar(45) NOT NULL,
  `NombreComuna` varchar(45) NOT NULL,
  PRIMARY KEY (`CódigoComuna`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comunas`
--

LOCK TABLES `comunas` WRITE;
/*!40000 ALTER TABLE `comunas` DISABLE KEYS */;
/*!40000 ALTER TABLE `comunas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contenidoslocales`
--

DROP TABLE IF EXISTS `contenidoslocales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contenidoslocales` (
  `IdContenidosLocales` int(11) NOT NULL,
  `nombre_sitio` varchar(45) NOT NULL,
  `url_sitio` varchar(45) NOT NULL,
  `url_ficha` varchar(45) NOT NULL,
  `RunPasaporte` varchar(45) NOT NULL,
  `CódigoSistema` varchar(45) NOT NULL,
  `CódigoComuna` varchar(45) NOT NULL,
  `Calificacion` varchar(45) NOT NULL,
  `fecha_ingreso` varchar(45) NOT NULL,
  `nombre_usuario` varchar(45) NOT NULL,
  `Estado` varchar(45) NOT NULL,
  `tipo_contenido` varchar(45) NOT NULL,
  `Observacion` varchar(45) NOT NULL,
  `Enviado` varchar(45) NOT NULL,
  `Id_portal` varchar(45) NOT NULL,
  PRIMARY KEY (`IdContenidosLocales`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contenidoslocales`
--

LOCK TABLES `contenidoslocales` WRITE;
/*!40000 ALTER TABLE `contenidoslocales` DISABLE KEYS */;
/*!40000 ALTER TABLE `contenidoslocales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `escolaridad`
--

DROP TABLE IF EXISTS `escolaridad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `escolaridad` (
  `CódigoEscoalridad` int(11) NOT NULL,
  `NombreEscolaridad` varchar(45) DEFAULT NULL,
  `OrdenEscolaridad` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`CódigoEscoalridad`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `escolaridad`
--

LOCK TABLES `escolaridad` WRITE;
/*!40000 ALTER TABLE `escolaridad` DISABLE KEYS */;
/*!40000 ALTER TABLE `escolaridad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evaluacioncontenido`
--

DROP TABLE IF EXISTS `evaluacioncontenido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evaluacioncontenido` (
  `idEvaluacion` int(11) NOT NULL,
  `idContenidoLocales` varchar(45) NOT NULL,
  `CodigoEncuestas` varchar(45) NOT NULL,
  `fechaIngreso` varchar(45) NOT NULL,
  `RunResponsable` varchar(45) NOT NULL,
  `Correo` varchar(45) NOT NULL,
  `IdRol` varchar(45) NOT NULL,
  `EsContenido` varchar(45) NOT NULL,
  `formato` varchar(45) NOT NULL,
  `PuntajeObt` varchar(45) NOT NULL,
  `PromObtenido` varchar(45) NOT NULL,
  `Tpreguntas` varchar(45) NOT NULL,
  `PuntajeEv2` varchar(45) NOT NULL,
  PRIMARY KEY (`idEvaluacion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evaluacioncontenido`
--

LOCK TABLES `evaluacioncontenido` WRITE;
/*!40000 ALTER TABLE `evaluacioncontenido` DISABLE KEYS */;
/*!40000 ALTER TABLE `evaluacioncontenido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formato (mcl)`
--

DROP TABLE IF EXISTS `formato (mcl)`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formato (mcl)` (
  `CódigoFormato` int(11) NOT NULL,
  `NombreFormato` varchar(45) NOT NULL,
  `Activo` varchar(45) NOT NULL,
  PRIMARY KEY (`CódigoFormato`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formato (mcl)`
--

LOCK TABLES `formato (mcl)` WRITE;
/*!40000 ALTER TABLE `formato (mcl)` DISABLE KEYS */;
/*!40000 ALTER TABLE `formato (mcl)` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ocupaciones (mu)`
--

DROP TABLE IF EXISTS `ocupaciones (mu)`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ocupaciones (mu)` (
  `CodigoActividad` int(11) NOT NULL,
  `NombreActividad` varchar(45) NOT NULL,
  PRIMARY KEY (`CodigoActividad`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ocupaciones (mu)`
--

LOCK TABLES `ocupaciones (mu)` WRITE;
/*!40000 ALTER TABLE `ocupaciones (mu)` DISABLE KEYS */;
/*!40000 ALTER TABLE `ocupaciones (mu)` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `origenusuario (mu)`
--

DROP TABLE IF EXISTS `origenusuario (mu)`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `origenusuario (mu)` (
  `CódigoOrigenUsuario` int(11) NOT NULL,
  `NombreOrigenUsuario` varchar(45) NOT NULL,
  PRIMARY KEY (`CódigoOrigenUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `origenusuario (mu)`
--

LOCK TABLES `origenusuario (mu)` WRITE;
/*!40000 ALTER TABLE `origenusuario (mu)` DISABLE KEYS */;
/*!40000 ALTER TABLE `origenusuario (mu)` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provincias`
--

DROP TABLE IF EXISTS `provincias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provincias` (
  `CódigoRegion` int(11) NOT NULL,
  `CódigoProvincia` varchar(45) NOT NULL,
  `NombreProvincia` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`CódigoProvincia`),
  CONSTRAINT `FK_Provincias_Regiones1` FOREIGN KEY (`CódigoProvincia`) REFERENCES `regiones` (`NombreRegion`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provincias`
--

LOCK TABLES `provincias` WRITE;
/*!40000 ALTER TABLE `provincias` DISABLE KEYS */;
/*!40000 ALTER TABLE `provincias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recintos`
--

DROP TABLE IF EXISTS `recintos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recintos` (
  `CódigoSistema` int(11) NOT NULL,
  `NombreRecinto` varchar(45) NOT NULL,
  `CódigoComuna` varchar(45) NOT NULL,
  `DireccionRecinto` varchar(45) NOT NULL,
  `CreacionRecinto` varchar(45) NOT NULL,
  PRIMARY KEY (`CódigoSistema`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recintos`
--

LOCK TABLES `recintos` WRITE;
/*!40000 ALTER TABLE `recintos` DISABLE KEYS */;
/*!40000 ALTER TABLE `recintos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `regiones`
--

DROP TABLE IF EXISTS `regiones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regiones` (
  `CódigoRegion` int(11) NOT NULL,
  `NombreRegion` varchar(45) NOT NULL,
  PRIMARY KEY (`NombreRegion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `regiones`
--

LOCK TABLES `regiones` WRITE;
/*!40000 ALTER TABLE `regiones` DISABLE KEYS */;
/*!40000 ALTER TABLE `regiones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sexo (mu)`
--

DROP TABLE IF EXISTS `sexo (mu)`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sexo (mu)` (
  `CodigoSexo` int(11) NOT NULL,
  `NombreSexo` varchar(45) NOT NULL,
  PRIMARY KEY (`CodigoSexo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sexo (mu)`
--

LOCK TABLES `sexo (mu)` WRITE;
/*!40000 ALTER TABLE `sexo (mu)` DISABLE KEYS */;
/*!40000 ALTER TABLE `sexo (mu)` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios (mu)`
--

DROP TABLE IF EXISTS `usuarios (mu)`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios (mu)` (
  `CodigoUsuario` int(11) NOT NULL,
  `RUN` varchar(45) NOT NULL,
  `DNI` varchar(45) NOT NULL,
  `Nombre` varchar(45) NOT NULL,
  `ApellidoPaterno` varchar(45) NOT NULL,
  `ApellidoMaterno` varchar(45) NOT NULL,
  `FechaNacimiento` datetime NOT NULL,
  `Correo` varchar(45) NOT NULL,
  `Fono` varchar(45) NOT NULL,
  `CodigoSexo` varchar(45) NOT NULL,
  `CodigoEscolaridad` varchar(45) NOT NULL,
  `CodigoOcupacion` varchar(45) NOT NULL,
  `CodigoComuna` varchar(45) NOT NULL,
  `NombreCalle` varchar(45) NOT NULL,
  `NumeroCalle` varchar(45) NOT NULL,
  `Departamento` varchar(45) NOT NULL,
  `Verificado` bit(10) DEFAULT NULL,
  `Celular` varchar(45) NOT NULL,
  PRIMARY KEY (`CodigoUsuario`),
  UNIQUE KEY `CodigoUsuario_UNIQUE` (`CodigoUsuario`),
  CONSTRAINT `FK_Usuarios_Comunas` FOREIGN KEY (`CodigoUsuario`) REFERENCES `comunas` (`CódigoComuna`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Usuarios_Escolaridad` FOREIGN KEY (`CodigoUsuario`) REFERENCES `escolaridad` (`CódigoEscoalridad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Usuarios_Ocupaciones` FOREIGN KEY (`CodigoUsuario`) REFERENCES `ocupaciones (mu)` (`CodigoActividad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Usuarios_Sexo` FOREIGN KEY (`CodigoUsuario`) REFERENCES `sexo (mu)` (`CodigoSexo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios (mu)`
--

LOCK TABLES `usuarios (mu)` WRITE;
/*!40000 ALTER TABLE `usuarios (mu)` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios (mu)` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuariosclaves (mu)`
--

DROP TABLE IF EXISTS `usuariosclaves (mu)`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuariosclaves (mu)` (
  `CódigoClave` int(11) NOT NULL,
  `CódigoUsuario` varchar(45) NOT NULL,
  `CódigoOrigenUsuario` varchar(45) NOT NULL,
  `RunPasaporte` varchar(45) NOT NULL,
  `ClaveUsuario` varchar(45) NOT NULL,
  `usuariosclaves (mu)col` varchar(45) NOT NULL,
  `usuariosclaves (mu)col1` varchar(45) DEFAULT NULL,
  `usuariosclaves (mu)col2` varchar(45) DEFAULT NULL,
  `usuariosclaves (mu)col3` varchar(45) DEFAULT NULL,
  `usuariosclaves (mu)col4` varchar(45) DEFAULT NULL,
  `usuariosclaves (mu)col5` varchar(45) DEFAULT NULL,
  `usuariosclaves (mu)col6` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`CódigoClave`,`CódigoUsuario`,`CódigoOrigenUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuariosclaves (mu)`
--

LOCK TABLES `usuariosclaves (mu)` WRITE;
/*!40000 ALTER TABLE `usuariosclaves (mu)` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuariosclaves (mu)` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'bd_drupal_ext'
--

--
-- Dumping routines for database 'bd_drupal_ext'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-05 14:17:37
